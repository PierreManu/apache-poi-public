package word;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class JavaWord {

	/**
	 * 
	 * @author Anthony Couture
	 * @param args
	 * @throws IOException
	 * 
	 * <p>
	 * 		Cette procedure permet d'executer cette classe. <br>
	 * 		Elle realise les actions suivantes : 
	 * 		<ul>
	 * 			<li>Creation du Word</li>
	 * 			<li>Creation d'un paragraphe</li>
	 * 			<li>Creer l'objet run qui permet d'ajouter du texte au paragraphe par exemple</li>
	 * 			<li>Ajout du texte "AHAH" dans le paragraphe</li>
	 *			<li>Creation du fichier, ecriture des donnees a l'interieur</li>
	 *			<li>Message de fin avec le chemin du fichier</li>
	 * 		</ul>
	 * </p>
	 */
	public static void main(String[] args) throws IOException {
		XWPFDocument document= new XWPFDocument();
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run=paragraph.createRun();
		run.setText("AHAH");
		
		
		File file = new File("./docWord.docx");
		file.getParentFile().mkdirs();
		FileOutputStream outFile = new FileOutputStream(file);
		document.write(outFile);
		System.out.println("Created file: " + file.getAbsolutePath());
		outFile.close();
	}
}
