# Introduction
Ce projet permet de savoir comment créer un fichier excel avec la librairie apache poi.  
La javadoc est dans le dossier **documentation**.

# Créer un Excel en Java
Le code source se trouve dans **src/excel/JavaExcel.java**  
Un fichier de sorti réalisé à partir du code source est disponible sous le nom **docExcel.xlsx**.  

Cette classe permet de réaliser ces actions :  
- Créer une document excel
- Créer une feuille à ce document
- D'initialiser la feuille selon un nombre de lignes et de colonnes stockés dans une varible
- D'insérer des entiers
- D'insérer des chaines de caractères
- D'insérer des formules
- D'insérer des sommes en €
- D'insérer des formules pour avoir le résultat en €
- De mettre en couleur une cellule
- De mettre les bordures à une cellule
- De rajouter une ligne après l'initialisation
- De rajouter une colonne après l'initialisation
- Créer le fichier de sorti
- Écrire le document excel à l'intérieur du fichier

# Créer un Word en Java
Le code source se trouve dans **src/word/JavaWord.java**  
Un fichier de sorti réalisé à partir du code source est disponible sous le nom **docWord.docx**.  

Cette classe permet de réaliser ces actions :  
- Créer un document Word
- Créer un paragraphe
- Créer l'objet run qui permet d'ajouter du texte au paragraphe par exemple
- Ajout du texte "AHAH" dans le paragraphe
- Création du fichier, écriture des données a l'intérieur
- Message de fin avec le chemin du fichier